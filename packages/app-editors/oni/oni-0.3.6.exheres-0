# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require desktop-utils gtk-icon-cache freedesktop-desktop

SUMMARY="An IDE powered by Neovim"
HOMEPAGE="https://www.onivim.io/"
DOWNLOADS="
platform:amd64? ( https://github.com/onivim/oni/releases/download/v${PV}/Oni-${PV}-x64-linux.tar.gz )
    https://aur.archlinux.org/cgit/aur.git/plain/icons.tar.gz?h=oni -> oni-icons.tar.gz
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( platform: amd64 )
"

DEPENDENCIES="
    run:
        app-editors/neovim
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        gnome-platform/GConf:2
        media-libs/fontconfig
        net-print/cups
        sys-apps/dbus
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libXScrnSaver
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/libxkbfile
        x11-libs/pango
"

BUGS_TO="hasufell@posteo.de"
REMOTE_IDS="github:onivim/${PN}"
UPSTREAM_DOCUMENTATION="https://onivim.github.io/oni-docs/"

WORK=${WORKBASE}/Oni-${PV}-x64-linux

pkg_setup() {
    exdirectory --allow /opt
}

src_install() {
    insinto /opt/${PN}
    doins -r *
    chmod +x "${IMAGE}"/opt/${PN}/oni

    herebin ${PN} <<EOF
#!/bin/bash
set -e

cd /opt/${PN}
exec ./oni "\$@"
EOF

    install_desktop_entry \
        "MimeType=text/english;text/plain;text/markdown;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;text/x-ruby;text/x-python;" \
        "Categories=Utility;TextEditor;" \
        "Keywords=Text;Editor;Neovim;" \
        "GenericName=Text Editor"

    for i in 16x16 32x32 64x64 128x128 256x256 512x512 1024x1024; do
        insinto /usr/share/icons/hicolor/${i}/apps
        newins "${WORKBASE}"/icons/${i}.png oni.png
    done
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

